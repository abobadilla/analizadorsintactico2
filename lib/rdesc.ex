defmodule Rdesc do


  def benchee() do
    Benchee.run(%{ "seq" => fn-> Rdesc.seq("data") end, "conc" => fn-> Rdesc.conc("data") end},
      formatters: [
        Benchee.Formatters.HTML,
        Benchee.Formatters.Console
      ]
    )
  end

  
   def seq(path \\ ".") do
    cond do
      File.regular?(path) -> [path]
      File.dir?(path) ->
        File.ls!(path)
        |> Enum.map(&Path.join(path, &1))
        |> Enum.map(&seq/1)
        |> Enum.concat
        |> Enum.map(fn x -> 
          if (File.regular?(to_string(x))) do
            onlyFileName = String.split(x, ".", trim: true) |> Enum.at(0)
            fileHtml = "HTMLs/" <> Path.basename(onlyFileName) <> ".html"
            File.write(fileHtml, "")
            {:ok, file} = File.open(fileHtml, [:write])
            IO.write(file," <link rel='stylesheet' href='../css/styles.css'>")
            IO.write(file,"<pre>")
            arr = File.read(x) |> elem(1) |> String.to_charlist() |> :lexer.string() |> elem(1)
            control(file, arr )
            IO.write(file,"</pre>")
          end
        end)
      true -> []
    end
  end

  def createHtml(lista) do
    Enum.map(lista,fn x ->
      if (File.regular?(to_string(x))) do
        onlyFileName = String.split(x, ".", trim: true) |> Enum.at(0)
        fileHtml = "HTMLs/" <> Path.basename(onlyFileName) <> ".html"
        File.write(fileHtml, "")
        {:ok, file} = File.open(fileHtml, [:write])
        IO.write(file," <link rel='stylesheet' href='../css/styles.css'>")
        IO.write(file,"<pre>")
        arr = File.read(x) |> elem(1) |> String.to_charlist() |> :lexer.string() |> elem(1)
        control(file, arr )
        IO.write(file,"</pre>")
      end
    end)
  end

  def cores(lista) do
    Enum.chunk_every(lista, div(length(lista), 8))
    |> Enum.map(fn parte -> Task.async(fn -> createHtml(parte)  end ) end)
    |> Enum.map(fn task -> Task.await(task) end)
    lista
  end

  def conc(path \\ ".") do
    cond do
      File.regular?(path) -> [path]
      File.dir?(path) ->
        File.ls!(path)
        |> Enum.map(&Path.join(path, &1))
        |> Enum.map(&seq/1)
        |> Enum.concat
        |> cores()
      true -> []
    end
  end


  def control(_,[]), do: true
  def control(file,[{token,_,chars}|tail]) do
    aux(file,token,chars)
    if (Enum.member?(['def','defmodule'],chars)) do
      auxFuncion(file,tail)
    else
      control(file,tail)
    end
  end

  def auxFuncion(_,[]), do: true
  def auxFuncion(file,[{_,_,'('}|tail]) do
    IO.write(file, "<span style='color:white;'>"<> to_string('(') <> "</span>")
    auxParametros(file, tail)
  end
  def auxFuncion(file,[{_,_,chars}|tail]) do
    if (Enum.member?(['do:', 'do'], chars)) do
      IO.write(file, "<span class='keyword'>"<> to_string(chars) <> "</span>")
      control(file,tail)
    else
      IO.write(file, "<span class='functionName'>"<> to_string(chars) <> "</span>")
      auxFuncion(file,tail)
    end
  end

  def auxParametros(_,[]), do: true
  def auxParametros(file,[{_,_,')'}|tail]) do
    IO.write(file, "<span style='color:white;'>"<> to_string(')') <> "</span>")
    auxFuncion(file, tail)
  end
  def auxParametros(file,[{token,_,chars}|tail]) do
    aux2(file, token, chars)
    auxParametros(file, tail)
  end

  def aux2(file,:atomo, chars), do: IO.write(file, "<span class='atomo'>"<> to_string(chars) <> "</span>")
  def aux2(file,:dot, chars), do: IO.write(file, "<span class='gray'><i> "<> to_string(chars) <> " </i></span>")
  def aux2(file,_, chars), do: IO.write(file, "<span class='parameter'><i>"<> to_string(chars) <> "</i></span>")
  def aux(file,:indentado, chars), do:  IO.write(file, to_string(chars))
  def aux(file,:atomo, chars), do: IO.write(file, "<span style='color:yellow;'>"<> to_string(chars) <> "</span>")
  def aux(file,:comment, chars), do: IO.write(file, "<span class='comment'>"<> to_string(chars) <> "</span>")
  def aux(file,:pipe,_), do: IO.write(file, "<span class='keyword'>|></span>")
  def aux(file,:keyword, chars), do: IO.write(file, "<span class='keyword'>"<> to_string(chars) <> "</span>")
  def aux(file,:bool, chars), do: IO.write(file, "<span style='color:darkcyan;'><i>"<> to_string(chars) <> "</i></span>")
  def aux(file,:dot, chars), do: IO.write(file, "<span class='gray'>" <> to_string(chars) <> "</span>")
  def aux(file,:function, chars), do: IO.write(file, "<span class='defFunction'>"<> to_string(chars) <> "</span>")
  def aux(file,:modulo, chars), do: IO.write(file, "<span style='color:peru;'>"<> to_string(chars) <> "</span>")
  def aux(file,:fecha, chars), do: IO.write(file, "<span style='color:pink;'>"<> to_string(chars) <> "</span>")
  def aux(file,:bitwise, chars), do: IO.write(file, "<span style='color:crimson;'>"<> to_string(chars) <> "</span>")
  def aux(file,_, chars), do:  IO.write(file, "<span class='gray2'>" <> to_string(chars) <> "</span>")
end
